#!/usr/bin/python
# -*- coding: utf-8 -*-
import helpers as H

class Relation(object):
    """ Object represent a relational database with its operations """

    def __init__(self, attributes):
        """ Initialize a Relation object with no tuple """
        if H.isinstanceof(attributes, Attribute) and len(attributes) > 0:
            self.attributes = attributes
            self.tuples = []
        else:
            raise AttrError

    def arity(self):
        """ Return relation's arity """
        return len(self.attributes)

    def get_attributes(self):
        """ Return relation's attributes as a list """
        return self.attributes[:]

    def get_tuples(self):
        """ Return relation's tuples as a list of tuples """
        return self.tuples[:]

    def __append_one(self, tpl, count):
        """ Add a tuple to the relation  """
        if len(tpl) == self.arity():
            if not tpl in self.tuples:
                self.tuples.append(tpl)
        else:
            for i in xrange(count):
                self.tuples.pop(len(self.tuples) - 1)
            raise TupleError

    def append(self, tuples):
        """ Add one tuples or a list of tuples to the relation
        through the Relation.__append(self, tpl) function """
        if isinstance(tuples, list):
            for i, tpl in enumerate(tuples):
                self.__append_one(tpl, i)
        elif isinstance(tuples, tuple):
            self.__append_one(tuples, 0)
        else:
            raise TypeError

    def selection(self, equality):
        """ Return a relation which its tuples respect
        the criteria <equality> """
        out = Relation(self.attributes)
        out_tuples = []
        index_self = self.attributes.index(equality[0])
        if isinstance(equality[1], Attribute):
            index_other = self.attributes.index(equality[1])
            out_tuples = [tpl for tpl in self.tuples
                          if tpl[index_self] == tpl[index_other]]
        else:
            out_tuples = [tpl for tpl in self.tuples
                          if tpl[index_self] == equality[1]]
        out.append(out_tuples)
        return out

    def projection(self, attributes):
        """ Extracts the specified attributes from a tuple or set of tuples """
        if H.isinstanceof(attributes, Attribute):
            out = Relation(attributes)
            out_t_i = [self.attributes.index(tpl) for tpl in attributes]
            for tpl in self.tuples:
                tmp = ()
                for i in out_t_i:
                    tmp += (tpl[i],)
                out.append(tmp)
            return out
        else:
            raise AttrError

    def join(self, relation):
        """ Connect two relations with their common attributes  """
        def __get_same_attributes(attributes):
            """ Return the list of common attributes """
            out = []
            for att in attributes[0]:
                if att in attributes[1]:
                    out.append(att)
            return out

        def __rename_common_attributes(attributes):
            """ Return a list which its elements are renamed if they
            are common with the main relation """
            out = []
            for att in attributes[1]:
                if att in attributes[0]:
                    out.append(Attribute(att + '_tmp'))
                else:
                    out.append(att)
            return out

        attributes = (self.attributes, relation.get_attributes())
        common_att = __get_same_attributes(attributes)
        tmp_att = __rename_common_attributes(attributes)
        all_tmp_att = self.attributes + tmp_att
        final_att = [att for att in all_tmp_att if att[-4:] != '_tmp']

        if len(self.tuples) != 0 and len(relation.get_tuples()) != 0:
            out = Relation(all_tmp_att)
            for tpl1 in self.tuples:
                for tpl2 in relation.get_tuples():
                    out.append(tpl1 + tpl2)
            if len(common_att) != 0:
                for att in common_att:
                    tmp_att = Attribute(att + '_tmp')
                    out = out.selection((att, tmp_att))
                out = out.projection(final_att)
            return out
        else:
            return Relation(final_att)

    def rename(self, attribute, new_attribute):
        """ Return a copy of the relation wich is attribute <attribute>
        is renamed by <new_attribute> """
        if isinstance(attribute, Attribute):
            out = self.copy()
            index = out.get_attributes().index(attribute)
            out.attributes[index] = new_attribute

            return out
        else:
            raise AttrError

    def union(self, relation):
        """ Combines the tuples of two relations and removes
        all duplicate tuples from the result """
        attributes = (self.attributes, relation.get_attributes())

        if H.has_same_attributes(attributes):
            out = self.copy()
            permut_array = H.get_permutation(attributes)

            for tpl in relation.get_tuples():
                sorting_t = H.sort_tuple(permut_array, tpl)
                out.append(sorting_t)
            return out
        else:
            raise AttrError

    def difference(self, relation):
        """ Produces the set of tuples from the first relation that do not
        exist in the second relation """
        out = self.copy()
        attributes = (self.attributes, relation.get_attributes())
        permut_array = H.get_permutation(attributes)

        if H.has_same_attributes(attributes):
            for tpl in relation.get_tuples():
                sorting_t = H.sort_tuple(permut_array, tpl)
                if sorting_t in out.get_tuples():
                    out.tuples.remove(sorting_t)
        return out

    def copy(self):
        """ Return a copy of the relation """
        out = Relation(self.get_attributes())
        out.append(self.get_tuples())
        return out

    def __str__(self):
        """ Return a string representation of the relation """
        def __get_separator(max_len, nb_el, char):
            """ Return a separator for the relation's elements """
            out = ""
            for i in xrange(nb_el):
                out += '+'
                out += char * (max_len[i] + 2)
            return out + "+\n"

        def __get_max_length(attributes, tuples):
            """ Return a list represent the max length of each column """
            out = [len(tpl) for tpl in attributes]
            for i in xrange(len(attributes)):
                for j in xrange(len(tuples)):
                    out[i] = max(out[i], len(str(tuples[j][i])))
            return out

        def __adjust_tuple(tuples):
            """ Add space after each element of a tuple for a nice print """
            out = []
            for i, tpl in enumerate(tuples):
                s = str(tpl)
                out.append(s + ' ' * (abs(max_len[i] - len(s))))
            return out

        out = ""
        max_len = __get_max_length(self.attributes, self.tuples)

        out += __get_separator(max_len, len(self.attributes), '-')
        out += '| ' + ' | '.join(__adjust_tuple(self.attributes)) + ' |\n'
        out += __get_separator(max_len, len(self.attributes), '=')

        for tpl in self.tuples:
            out += '| ' + ' | '.join(__adjust_tuple(tpl)) + ' |\n'

        out += __get_separator(max_len, len(self.attributes), '-')

        return out

    def __eq__(self, other):
        """ Verify the equality of two relations """
        if isinstance(other, Relation):
            if self.arity() == other.arity() \
            and len(self.tuples) == len(other.get_tuples()):
                attributes = (self.attributes, other.get_attributes())
                permut_array = H.get_permutation(attributes)
                for tpl in other.get_tuples():
                    sorting_t = H.sort_tuple(permut_array, tpl)
                    if not sorting_t in self.tuples:
                        return False
                return True
            return False
        else:
            raise RelError

# Note to students: You should NOT modify what follows!

class Error(Exception):
    """ Base class for all exceptions in this module. """
    pass


class AttrError(Error):
    """ Exception raised for errors related to an attribute. """
    pass


class RelError(Error):
    """ Exception raised for errors related to a relation. """
    pass


class Attribute(str):
    """ Define an Attribute as a subclass of str, mainly for type checking. """
    pass

class TupleError(Error):
    """ Exception raised for errors related to an tuple """
    pass
