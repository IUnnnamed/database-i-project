from relation import Relation as Rel
from relation import Attribute as Att

def test_arity(relation):
    print 'Test Relation.arity()'
    print str(relation.arity()) + '\n'

def test_get_attributes(relation):
    print 'Test Relation.get_attributes()'
    print str(relation.get_attributes()) + '\n'

def test_get_tuples(relation):
    print 'Test Relation.get_tuples()'
    print str(relation.get_tuples()) + '\n'

def test_append(relation, tuples):
    print 'Test Relation.append(tuples)\nBefore'
    print relation
    out = relation.copy()
    out.append(tuples)
    print 'After'
    print out
    return out

def test_selection(relation, equality):
    print 'Test Relation.selection(equality)\nBefore'
    print relation
    out = relation.selection(equality)
    print 'After'
    print out
    return out

def test_projection(relation, attributes):
    print 'Test Relation.projection(attributes)\nBefore'
    print relation
    out = relation.projection(attributes)
    print 'After'
    print out
    return out

def test_join(relations):
    print 'Test Relation.join(relation)\nBefore'
    print relations[0]
    out = relations[0].join(relations[1])
    print 'After'
    print out
    return out

def test_rename(relation, att, new_att):
    print 'Test Relation.rename(attributes, new_attributes)\nBefore'
    print relation
    out = relation.rename(att, new_att)
    print 'After'
    print out
    return out

def test_copy(relation):
    print 'Test Relation.copy()'
    print relation
    out = relation.copy()
    print 'After'
    print out
    return out

def test_difference(relations):
    print 'Test Relation.difference()'
    print relations[0]
    out = relations[0].difference(relations[1])
    print 'After'
    print out
    return out

def test_union(relations):
    print 'Test Relation.union()'
    print relations[0]
    print relations[1]
    out = relations[0].union(relations[1])
    print 'After'
    print out
    return out

if __name__ == "__main__":
    attributes = [[Att('a'),Att('b'),Att('c'),Att('d')],
                  [Att('d'),Att('f'),Att('g'),Att('h')]]
    relations = [Rel(attributes[0]),Rel(attributes[0])]

    tuples = [[('1','2','3','4'), ('2','3','4','5'),
               ('3','4','5','6'), ('4',5,'6','7')],
              [('5','6','6','5'), ('2','2','2','2'),
               ('2','3','4','5')]]

    test_arity(relations[0])
    test_get_attributes(relations[0])
    test_get_tuples(relations[0])
    test_append(relations[0], tuples[0])

    relations[0].append(tuples[0])
    test_selection(relations[0], (Att('b'), '5'))
    test_selection(relations[0], (Att('a'), '6'))
    test_selection(relations[0], (Att('b'), Att('c')))

    test_projection(relations[0], [Att('d'),Att('c')])
    test_rename(relations[0], Att('a'), Att('g'))

    test_copy(relations[0])

    relations[1].append(tuples[1])

    test_join(relations)

    test_difference(relations)
    test_union(relations)
