# -*- coding: utf-8 -*-

def isinstanceof(sequence, type):
    """ Verify if all elements of a sequence has <type> type """
    for el in sequence:
        if not isinstance(el, type):
            return False
    return True

def has_same_attributes(attributes):
    """ Verify of two list has the same elements, no matter the order """
    if len(attributes[0]) == len(attributes[1]):
        for att in attributes[0]:
           if not att in attributes[1]:
               return False
        return True
    else:
        return False

def get_permutation(attributes):
    """ Return a list represent the permutation to do for having
    the elements of attributes are in the same order"""
    out = []
    for att in attributes[0]:
        out.append(attributes[1].index(att))
    return out

def sort_tuple(permutation, tuple):
    """ Return a list which its elements are sorting with the help
    of the permutation's list"""
    out = ()
    for i in range(len(tuple)):
        out += (tuple[permutation[i]],)
    return out
